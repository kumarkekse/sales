require_relative 'product'
class TaxCalculationApp
  def self.display_user_options
    puts "\n\n\nPlease Choose your option:\n\n
    Enter 1:\n
      To see list of products.\n\n
    Enter 2:\n
      To purchase products.\n\n
    Enter 3:\n
      To Exit.\n\n
    Enter the operation no. you want to perform."
    choice = gets.chomp.to_i
    perform_operation(choice)
  end

  def self.greet_user
    puts "\n\n\n               Welcome To Store\n\n\n"
  end

  def self.perform_operation(choice)
    case choice
    when 1
      puts "\n\n\n Product ID,   Product Name,     Price\n\n"

      Product::LIST.each do |product|
        puts " #{product[:id]},   #{product[:name]},      #{product[:price]}\n"
      end
      display_user_options
    when 2
      Product.purchase_products
    when 3
      thanks_to_user
    else
      puts 'Please choose correct Option.'
      display_user_options
    end
  end

  def self.thanks_to_user
    puts 'Thanks for using our Store!!!'
  end
end

TaxCalculationApp.greet_user
TaxCalculationApp.display_user_options
TaxCalculationApp.thanks_to_user