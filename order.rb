require_relative 'product'
class Order
  def initialize
    @order_items = []
  end

  attr_accessor :order_items

  def add_product_to_order
    product_id, quantity = ask_product_details
    order_item = Product::LIST.select { |a| a[:id].to_i == product_id }[0]
    return unless order_item

    add_item(order_item, quantity)
    display_order
    puts "\n\n\nWant to add more products? answer with 'yes' or 'no'.\n\n\n"
    add_more_product = gets.chomp
    if add_more_product == 'yes'
      add_product_to_order
    else
      generate_bill
    end
  end

  def add_item(order_item, quantity)
    order_item[:quantity] = quantity
    order_item[:price_with_tax] = (order_item[:price] * quantity)
    @order_items << order_item
  end

  def generate_bill
    apply_taxes
    puts "\n\n\n     Reciept\n\n\n"
    puts 'Quantity,   Product Name ,   Price'
    @order_items.each do |order_item|
      puts "#{order_item[:quantity]}, #{order_item[:name]}, #{sprintf('%.2f', (order_item[:price_with_tax]))}\n"
    end
    total_price_with_tax =  @order_items.map { |a| a[:price_with_tax] }.inject(0, &:+)
    total_price = @order_items.map { |a| a[:price] }.inject(0, &:+)
    total_tax = sprintf('%.2f', (total_price_with_tax - total_price))
    puts "\n\n\nSales Taxes: #{total_tax}\n\nTotal: #{sprintf('%.2f', (total_price_with_tax))}"
    [total_price_with_tax, total_tax]
  end

  def ask_product_details
    puts "\n\n\nEnter Product Id to add product to the list\n\n"
    product_id = gets.chomp.to_i
    puts "\n\nEnter quantity\n\n"
    quantity = gets.chomp.to_i
    if !Product::LIST.map { |a| a[:id] }.include?(product_id) || quantity <= 0
      puts "\n\n Product Not Found Or Quantity is not correct\n\n"
      add_product_to_order
    else
      [product_id, quantity]
    end
  end

  def display_order
    puts "\n\n\n      Your order Items\n\n\n"
    puts 'Quantity,   Product Name ,   Price'
    @order_items.each do |order_item|
      puts "#{order_item[:quantity]}, #{order_item[:name]}, #{sprintf('%.2f', (order_item[:quantity] * order_item[:price]))}\n"
    end
  end

  def apply_taxes
    @order_items.each do |order_item|
      price = order_item[:price] * order_item[:quantity]
      unless Product::CATEGORY_FOR_BASIC_TAX_NA.include?(order_item[:category])
        order_item[:price_with_tax] += ((price * Product::BASIC_SALES_TAX) / 100)
      end

      if order_item[:imported]
        order_item[:price_with_tax] += ((price * Product::IMPORT_TAX) / 100)
      end
      order_item[:price_with_tax] = order_item[:price_with_tax].round(2)
    end
  end
end