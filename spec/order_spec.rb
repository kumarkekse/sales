require './order'
RSpec.describe Order do
  let(:order) { described_class.new }
  
  before do
    Product::LIST.first(3).each { |product| order.add_item(product, 1) }
  end

  describe 'it should initialize order_items with a blank array' do
    it { expect(described_class.new.order_items).to_not eq(nil) }
  end

  describe 'it should add product to order' do
    it { expect(order.order_items.length).to eq 3 }
  end

  describe 'apply taxes' do
    it 'should apply taxes' do
      order.apply_taxes
      product = order.order_items.select { |p| p[:category] == 'NA' }[0]
      expect(product[:price_with_tax]).to be > product[:price]
    end
  end

  describe 'generate total with taxes' do
    it 'should return total price and taxes' do
      billing_array = order.generate_bill
      total_price_with_tax = order.order_items.map do |o|
        o[:price_with_tax]
      end.sum
      total_price = order.order_items.map do |o|
        o[:price]
      end.sum
      total_tax = sprintf('%.2f', (total_price_with_tax - total_price).round(2))
      expect(billing_array).to eq [total_price_with_tax, total_tax]
    end
  end

  describe 'generat_bill' do
    it 'display reciept' do
      res = "\n\n\n     Reciept\n\n\nQuantity,   Product Name ,   Price\n1, book, 12.49\n1, music cd, 16.49\n1, chocolate bar, 0.85\n\n\n\nSales Taxes: 1.50\n\nTotal: 29.83\n"
      expect { order.generate_bill }.to output(res).to_stdout
    end
  end
end
