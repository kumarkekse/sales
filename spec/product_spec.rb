require './product'
RSpec.describe Product do
  describe 'it should have neccessary constants' do
    it { expect(described_class).to be_const_defined(:LIST) }
    it { expect(described_class).to be_const_defined(:BASIC_SALES_TAX) }
    it { expect(described_class).to be_const_defined(:IMPORT_TAX) }
    it { expect(described_class).to be_const_defined(:CATEGORY_FOR_BASIC_TAX_NA) }
  end

  describe 'list of category for basic sales tax is not applicable' do
    it { expect(described_class::CATEGORY_FOR_BASIC_TAX_NA).to include('book') }
  end

  describe 'it should match the products count' do
    it { expect(described_class::LIST.length).to eq 9 }
  end

  describe 'it should match the basic sales tax' do
    it { expect(described_class::BASIC_SALES_TAX).to eq 10 }
  end

  describe 'it should match the import goods tax' do
    it { expect(described_class::IMPORT_TAX).to eq 5 }
  end
end
