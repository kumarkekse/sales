require_relative 'order'
class Product
  CATEGORY_FOR_BASIC_TAX_NA = %w(food medical book)
  BASIC_SALES_TAX = 10
  IMPORT_TAX = 5
  LIST = [
    { id: 1, name: 'book', category: 'book', imported: false, price: 12.49 },
    { id: 2, name: 'music cd', category: 'NA', imported: false, price: 14.99 },
    { id: 3, name: 'chocolate bar', category: 'food', imported: false, price: 0.85 },
    { id: 4, name: 'imported box of chocolates', imported: true, category: 'food', price: 10.00 },
    { id: 5, name: 'imported bottle of perfume', imported: true, category: 'NA', price: 47.50 },
    { id: 6, name: 'bottle of perfume', imported: false, category: 'NA', price: 18.99 },
    { id: 7, name: 'packet of headache pills', imported: false, category: 'medical', price: 9.75 },
    { id: 8, name: 'box of imported chocolates', imported: true, category: 'food', price: 11.25 },
    { id: 9, name: 'imported bottle of perfume', imported: true, category: 'NA', price: 27.99 }
  ]

  def self.purchase_products
    puts "\n\n\nWant to add products to Order? answer with 'yes' or 'no'.\n\n
    Please Enter choice...\n\n"
    case gets.chomp
    when 'yes'
      Order.new.add_product_to_order
    when 'no'
      return
    else
      puts "Please choose correct Option:\n\n\n"
      purchase_products
    end
  end
end